import React from 'react';
import * as imageAction from '../action/images.action'
import { put, takeLatest } from 'redux-saga/effects'
import { getApi} from '../utils/ApiRequest';

/////////////////// Fetch List////////////////////

function* fetchImage() {
    try {
        const jsonData = yield getApi("list")
        yield put({ type: imageAction.IMAGE_LIST_SUCCESS, response: jsonData, status: imageAction.IMAGE_LIST_SUCCESS })
    } catch (error) {
        yield put({ status: imageAction.IMAGE_LIST_FALIURE, response: error, type: imageAction.IMAGE_LIST_FALIURE })
    }
}

export function* fetchImageSagaIndex() {
    yield takeLatest(imageAction.IMAGE_LIST_REQUEST, fetchImage)
}

