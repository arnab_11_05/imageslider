import React from 'react';
import { all } from 'redux-saga/effects';
import { fetchImageSagaIndex } from './picture.saga'

export default function* rootSaga() {
    console.log("all called")
    yield all([
        fetchImageSagaIndex(),
    ])
}