import React, { useCallback } from 'react'
import { View, TouchableOpacity, Image, ImageBackground, StyleSheet, Text } from 'react-native'
import ImagePath from '../utils/ImagePath'

function ImageHolder(props) {

    const onMoveForward = useCallback(() => {
        props.onMoveForward()
    });
    const onMoveBackward = useCallback(() => {
        props.onMoveBackward()
    });

    return (
        <View style={styles.container}>
            <Text>{props.authorName}</Text>

            <ImageBackground style={styles.imageContainer}
                source={{ uri: props.imageData }}
                loadingIndicatorSource={ImagePath.loading_indicator}
            >
                <View style={styles.navContainer}>
                    <TouchableOpacity onPress={onMoveBackward}>
                        <Image style={styles.navIcon} source={ImagePath.left_arrow} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onMoveForward}>
                        <Image style={styles.navIcon} source={ImagePath.right_arrow} />
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageContainer: {
        height: 200,
        width: 300,
        justifyContent: 'center'
    },
    navContainer: {
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    navIcon: {
        height: 20,
        width: 20,
    }
})

export default ImageHolder