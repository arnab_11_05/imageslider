import React, { Component } from 'react'
import {
    StyleSheet, ActivityIndicator,ScrollView,RefreshControl
} from 'react-native'
import { connect } from "react-redux";
import { fetchImageList, IMAGE_LIST_SUCCESS } from '../action/images.action'
import { getSpecificSizedImage, getRandomArbitrary } from '../utils/UtilMethods'
import ImageHolder from './ImageHolderComponent'

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            img_arr: [],
            currentIndex: 0,
            refreshing: false
        }
        this.moveForward = this.moveForward.bind(this)
        this.moveBackward = this.moveBackward.bind(this)
        this.onRefresh = this.onRefresh.bind(this)
    
    }

    componentDidMount() {
        this.props.fetchList()
    }

    /// A Function To Move Forward In The List/////
    moveForward() {
        if (this.state.currentIndex < this.state.img_arr.length - 1) {
            this.setState((state) => ({
                currentIndex: state.currentIndex + 1
            }))
        } else {
            this.setState({
                currentIndex: 0
            })
        }
    }

    /// A Function To Move Backword In The List/////
    moveBackward() {
        if (this.state.currentIndex > 0) {
            this.setState((state) => ({
                currentIndex: state.currentIndex - 1
            }))
        } else {
            this.setState({
                currentIndex: this.state.img_arr.length - 1
            })
        }
    }

    //////// Refresh Handler /////////////
    onRefresh(){
        this.setState({
            refreshing: true,
            currentIndex: parseInt(getRandomArbitrary(0, this.state.img_arr.length - 1))
        })
        this.refreshTimer=setTimeout(()=>{this.setState({refreshing: false})}, 1000)
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.pic_list_status !== "") {
            if (nextProps.pic_list_status == IMAGE_LIST_SUCCESS) {

                if (nextProps.pic_list_response.data !== prevState.img_arr) {
                    let randomIndex = parseInt(getRandomArbitrary(0, nextProps.pic_list_response.data.length - 1))
                    return {
                        img_arr: nextProps.pic_list_response.data,
                        currentIndex: randomIndex
                    }
                }
            }
        }

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.pic_list_response !== null) {
            if (this.props.pic_list_response.data !== prevState.img_arr) {
                this.timer = setInterval(this.moveForward, 3000)
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.timer)
        clearTimeout(this.refreshTimer)
    }

    render() {
        return (
            <ScrollView style={styles.container}
            refreshControl={
                <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
              }
            >
                {this.state.img_arr.length > 0 ?
                    <ImageHolder
                        authorName={this.state.img_arr[this.state.currentIndex].author}
                        imageData={getSpecificSizedImage(this.state.img_arr[this.state.currentIndex].id)}
                        onMoveForward={this.moveForward}
                        onMoveBackward={this.moveBackward}
                    />: <ActivityIndicator size="large" color="#000000" />}
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageContainer: {
        height: 200,
        width: 300,
        justifyContent: 'center'
    },
    navContainer: {
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    navIcon: {
        height: 20,
        width: 20,
    }
})

const mapStateToProps = state => {
    return {
        pic_list_status: state.imageReducer.pic_list_status,
        pic_list_response: state.imageReducer.pic_list_response,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchList: () => {
            dispatch(fetchImageList())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)