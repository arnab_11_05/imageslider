////////// Method For Generating Image Url From ID////
export function getSpecificSizedImage(id) {
    let imgUrl= "https://picsum.photos/200/300?image="+id
    return imgUrl
}

////////// Method For Generating Random Number In A Range////
export function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}