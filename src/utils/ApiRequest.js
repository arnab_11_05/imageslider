import axios from 'axios';

axios.defaults.baseURL= 'https://picsum.photos/';
axios.defaults.timeout=2500

export function getApi(url) {
    return axios.get(`/${url}`);
}