const ImagePath={
    left_arrow: require('../images/left.png'),
    right_arrow: require('../images/right.png'),
    loading_indicator: require('../images/loading.png')
}
export default ImagePath;