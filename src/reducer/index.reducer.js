import React from 'react';
import { combineReducers } from 'redux'
import imageReducer from './images.reducer'

const appReducer = combineReducers({
    imageReducer
})

export default appReducer