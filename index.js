/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import React from 'react';
import {name as appName} from './app.json';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from 'redux'
import appReducer from './src/reducer/index.reducer';
import createSagaMiddleware from 'redux-saga'
import rootSaga from './src/saga/index.saga'

const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
    appReducer,
    applyMiddleware(sagaMiddleware)
);
sagaMiddleware.run(rootSaga);
const reduxApp=()=>(
    <Provider store = {store}>
        <App/>
    </Provider>
)

AppRegistry.registerComponent(appName, () => reduxApp);
//AppRegistry.registerComponent(appName, () => App);
